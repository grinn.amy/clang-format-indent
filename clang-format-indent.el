;;; clang-format-indent.el --- Use clang-format to determine indentation -*- lexical-binding: t -*-

;; Copyright (C) 2024 Amy Grinn

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 1.0.0
;; File: clang-format-indent.el
;; Package-Requires: ((emacs "24.4") (clang-format "0.1"))
;; Keywords: tools
;; URL: https://gitlab.com/grinn.amy/clang-format-indent

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; `clang-format-indent-mode' advises the existing indentation
;; function using the `clang-format-executable'.  This can be turned
;; on automatically using a mode hook:
;;
;;   (add-hook 'c-mode-common-hook 'clang-format-indent-mode)

;;; Code:

(require 'clang-format)

(defun clang-format-indent--line (&rest _)
  "Indent current line using clang-format, if not on a blank line.

Returns nil on blank lines.  Discards replacements which affect
text beyond the end of the line."
  (when (save-excursion
          (beginning-of-line)
          (not (looking-at-p "[ \t]*$")))
    (prog1 t
      ;; Remove trailing whitespace
      (save-excursion
        (end-of-line)
        (delete-char (- (skip-chars-backward " \t"))))
      ;; Discard replacements which affect text beyond end of line
      (let* ((next-char (clang-format--bufferpos-to-filepos
                         (save-excursion
                           (end-of-line)
                           (skip-chars-forward " \t\r\n")
                           (point))
                         'exact 'utf-8-unix))
             (advice `(lambda (offset length &rest _)
                        (< (+ offset length) ,next-char))))
        (advice-add #'clang-format--replace :before-while advice)
        (unwind-protect
            (let ((inhibit-message t))
              (clang-format-region (line-beginning-position)
                                   (line-end-position)))
          (advice-remove #'clang-format--replace advice)))
      (when (< (current-column) (current-indentation))
        (back-to-indentation)))))

;;;###autoload
(define-minor-mode clang-format-indent-mode
  "Use clang-format to control indentation on contentful lines.

Clang format indent mode is a buffer-local minor mode.  When
enabled, indentation for lines that do not solely consist of
whitespace will be determined by running the buffer through the
`clang-format-executable' program.  On empty lines, the existing
indentation function will be used."
  :global nil :group 'tools
  (if clang-format-indent-mode
      (advice-add indent-line-function :before-until
                  #'clang-format-indent--line)
    (advice-remove indent-line-function #'clang-format-indent--line)))

(provide 'clang-format-indent)
;;; clang-format-indent.el ends here
